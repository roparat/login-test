import jf from './jf';
import { setToken } from './global';

const doLogin = (username, password) => {
    const data = {
        email: username || "roparat@gmail.com",
        password: password || "d",
    }

    const api = "/Users/login";

    return new Promise((resolve, reject) => {
        jf(api, 'post', data, {access_token:undefined})
            .then(jsondata => {
                console.log(jsondata);
                if (!jsondata.error) {
                    const token = jsondata.id;
                    const userId = jsondata.userId;
                    console.log(`Login pass with token: ${token} userId: ${userId}`);
                    setToken({ token, userId });
                    resolve({ token, userId });
                } else {
                    reject(jsondata.error);
                }

            })
            .catch(error => reject);
    });


}

const doRegister = (username, password) => {
    const data = {
        "email": username || "roparat@gmail.com",
        "password": password || "d"
    }

    const api = "/Users";

    return new Promise((resolve, reject) => {
        jf(api, 'post', data)
            .then(jsondata => {
                console.log('register pass');
                console.log(jsondata);
            })
            .catch(error => reject);
    });
}

export const login = doLogin;
export const register = doRegister;