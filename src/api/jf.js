import 'whatwg-fetch';
import {getToken} from './global';
const apipath = '//localhost:3008/api';

//accessTokens?access_token=NHg2CxBKgYPzOW2etGmq6DklyvGTQloOnfOhOYrdDLsGhCvsxeSxJFxHWZYjJts8

export default (restPath, method, json, queryparams) => {

    let qparam = queryparams || {};

    // if have access token, then add to qparam here.
    if (!qparam.access_token) {
        qparam = Object.assign({}, qparam, {access_token:getToken().token});
    }

    let qparamstring = Object.entries(qparam).filter(v=>v[1]!==undefined).map(value=>`${value[0]}=${value[1]}`).join('&');
    const path = apipath + restPath + ((qparamstring.length > 0) ? `?${qparamstring}`:'');

    console.log(`${method} - ${path}`);

    return fetch(path, {
        method,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: (json)?JSON.stringify(json):undefined,
    })
    .then(resp => { return resp.json() })
    .catch(error => {return Promise.reject(error)});
};