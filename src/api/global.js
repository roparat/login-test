export const setLocalStorage = (key,value) => {
    localStorage.setItem(key,value);
}

export const getLocalStorage = (key) => {
    const retval = localStorage[key];
    console.log(retval);
    return retval;
}

export const setToken = ({token,userId}) => {
    setLocalStorage('access-token', JSON.stringify({token,userId}));
}

export const getToken = () => {
    let retval = getLocalStorage('access-token');
    try {
        retval = JSON.parse(retval);
    } catch(e){
        retval = {};
    }
    return retval;
}