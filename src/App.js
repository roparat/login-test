import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import LoginController from './UserRoute/LoginController';
import RegisterController from './UserRoute/RegisterController';
import jf from './api/jf';

const doClick = () => {
  jf('/courses','get',null)
  .then(data=>console.log(data))
  .catch(error=>console.log(error));
}

class App extends Component {
  render() {
    return (
      <div className="App">
      <RegisterController/>
        <LoginController/>
        <button onClick={doClick} />
      </div>
    );
  }
}

export default App;
