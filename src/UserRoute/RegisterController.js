import React, { Component } from 'react';
import RegisterForm from './RegisterForm';
import * as UserAPI from '../api/user';

class RegisterController extends Component {
    constructor(props) {
        super(props);
        this.doRegister = this.doRegister.bind(this);
    }

    doRegister(username, password) {
        UserAPI.register(username,password).then((token,userid)=>{
            console.log('register pass');
        }).catch(error=>console.log(error));
    }

    render() {
        return (
            <div>
                <RegisterForm doRegister={this.doRegister} />
            </div>);
    }
}

export default RegisterController;