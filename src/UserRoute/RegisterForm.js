import React from 'react';
import LoginForm from './LoginForm';

const RegisterForm = (props) => <LoginForm doLogin={props.doRegister} buttonText="Register" />;

export default RegisterForm;