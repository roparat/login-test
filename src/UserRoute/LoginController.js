import React, { Component } from 'react';
import LoginForm from './LoginForm';
import * as UserAPI from '../api/user';

class LoginController extends Component {
    constructor() {
        super();
        this.doLogin = this.doLogin.bind(this);
    }

    doLogin(username, password) {
        UserAPI.login(username,password).then((token,userid)=>{

        }).catch(error=>console.log(error));
    }

    render() {
        return (
            <div>
                <LoginForm doLogin={this.doLogin} buttonText="Login"/>
            </div>);
    }
}

export default LoginController;