import React from 'react';

class LoginForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
        };
        this.handleUserName = this.handleUserName.bind(this);
        this.handlePassWord = this.handlePassWord.bind(this);
        this.onClick = this.onClick.bind(this);
    }

    handleUserName(e) {
        this.setState({
            username: e.target.value,
        });
    }

    handlePassWord(e) {
        this.setState({
            password: e.target.value,
        });
    }

    onClick(e){
        this.props.doLogin(this.state.username, this.state.password);
    }

    render() {
        console.log(this.props);
        return (
            <div>
                <input type="email" onChange={this.handleUserName} value={this.state.username} placeholder="email"/>
                <input type="password" onChange={this.handlePassWord} value={this.state.password} placeholder="enter your password..."/>
                <button onClick={this.onClick}>{this.props.buttonText}</button>
            </div>
        );
    }
}

LoginForm.propTypes = {
    buttonText: React.PropTypes.string,
    doLogin: React.PropTypes.func.isRequired,
};

export default LoginForm;